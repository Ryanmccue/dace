# README #

### Overview ###
For your hiring project, you will make a web app which creates and move "jobs" to and from different categories.

![Example from Jira](images/jobs.png)

### Requirements ###

* Email/password authentication with FireBase.
* Facebook authentication with Firebase
* Frontend must be written using React.

### Views ###

* Login view
* Register view
* Home page
* detailed individual job view
* View of all jobs and categories (See example image above)

### BONUS ###

Jobs can be drag and dropped into different categories

### Next Steps ###

* If you don't fully grasp how the Firebase database and queries work this is a good video series called `The Firebase Database For SQL Developers` (https://www.youtube.com/playlist?list=PLl-K7zZEsYLlP-k-RKFa7RyNPa9_wCH2s)
* Create a Firebase project (https://firebase.google.com/)
* Start coding
* Make commits in the repository.

If you have any questions email me at ryanm@freshworks.io!