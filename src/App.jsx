import React, { Component } from 'react';
import * as firebase from "firebase";
import Project from "./Project"
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

var config = {
  apiKey: "AIzaSyBaGTjRQatGQAmMbeNX8qDSvmxFoPy16Ck",
  authDomain: "job-tracker-57de8.firebaseapp.com",
  databaseURL: "https://job-tracker-57de8.firebaseio.com",
  projectId: "job-tracker-57de8",
  storageBucket: "job-tracker-57de8.appspot.com",
  messagingSenderId: "231420822119"
};
firebase.initializeApp(config);

const Home = () => (
  <div>
    <h2>Home</h2>
  </div>
)

const App = () => (
  <Router>
    <div>
    <span>Job Tracker</span>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/project">Project</Link></li>
      </ul>

      <hr/>

      <Route exact path="/" component={Home}/>
      <Route exact path="/project" component={Project}/>
    </div>
  </Router>
)
export default App